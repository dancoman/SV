import * as firebase from 'firebase'
import ApiKeys from '../api/ApiKeys'
import "firebase/firestore"

firebase.initializeApp(ApiKeys)
firebase.firestore().settings({ timestampsInSnapshots: true })

export default firebase
