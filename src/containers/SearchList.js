// eslint-disable-next-line prefer-stateless-function
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class SearchList extends Component {
  render() {
    return (
      <View style={styles.container} >
        <Text>List of Search Results</Text>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
