import React, { Component } from 'react'
import { View, Button, StyleSheet } from 'react-native'
import Expo from 'expo'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
// import { dispatch } from '../../App';

@firebaseConnect()

export default class Login extends Component {
  static navigationOptions = {
    header: null
  }

  async loginWithFacebook() {
    const data = await Expo.Facebook.logInWithReadPermissionsAsync('853187294888236', { permissions: ['public_profile', 'email'] })

    if (data.type === 'success') {
      const creds = this.props.firebase.auth.FacebookAuthProvider.credential(data.token)
      // console.log('TCL: Login -> asyncloginWithFacebook -> creds', creds);
      const response = await this.props.firebase.auth().signInAndRetrieveDataWithCredential(creds)
      // console.log('TCL: Login -> asyncloginWithFacebook -> response', response)
      const { name, email } = response.additionalUserInfo.profile
      // console.log('TCL: Login -> asyncloginWithFacebook -> name: ', name);
      await this.props.firebase.updateProfile({ name, email })
    }

    this.props.navigation.navigate('Dashboard')
  }


  render() {
    console.log('propsLogin: ', this.props)
    return (
      <View style={styles.base}>
        <Button title='Login' onPress={() => this.loginWithFacebook()} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  base: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})