import React, { Component } from 'react'
import {
  Image,
  StyleSheet,
  Text,
  Button,
  View,
  Dimensions,
  AsyncStorage
} from 'react-native'
import { connect } from 'react-redux';
import { compose } from 'redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { Icon } from 'react-native-elements'
import {
  TabView,
  SceneMap
} from 'react-native-tab-view'
import { persistor } from '../../App';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  headerContainer: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    marginBottom: 10,
    top: 30,
  },
  indicatorTab: {
    backgroundColor: 'transparent',
  },
  socialIcon: {
    marginLeft: 14,
    marginRight: 14,
  },
  socialRow: {
    flexDirection: 'row',
    marginBottom: 30,
  },
  tabBar: {
    backgroundColor: '#EEE',
  },
  tabContainer: {
    flex: 1,
    marginBottom: 12,
  },
  tabLabelNumber: {
    color: 'gray',
    fontSize: 12.5,
    textAlign: 'center',
  },
  tabLabelText: {
    color: 'black',
    fontSize: 22.5,
    fontWeight: '600',
    textAlign: 'center',
  },
  userBioRow: {
    marginLeft: 40,
    marginRight: 40,
  },
  userBioText: {
    color: 'gray',
    fontSize: 13.5,
    textAlign: 'center',
  },
  userImage: {
    borderRadius: 60,
    height: 120,
    marginBottom: 10,
    width: 120,
  },
  userNameRow: {
    marginBottom: 10,
  },
  userNameText: {
    color: '#5B5A5A',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  userRow: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: 12
  }
});

@firebaseConnect()
@connect(state => ({
  profile: state.firebase.auth
}))

export default class Profile extends Component {
  static navigationOptions = {
    headerTransparent: true
  }

  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'First' },
      { key: 'second', title: 'Second' },
    ],
  };

  firstRoute = () => (
    <View style={[styles.container, { backgroundColor: '#ff4081' }]} >
      <Button
        title='Logout' 
        onPress={() => {
          AsyncStorage.clear()
          this.props.navigation.navigate('Login')
        }}
      />
    </View>
  );

  secondRoute = () => (
    <View style={[styles.container, { backgroundColor: '#673ab7' }]} />
  );

  renderContactHeader = () => {
    const { profile } = this.props
    const image = profile.photoURL ? profile.photoURL : 'https://pbs.twimg.com/profile_images/909953369694859265/BOakwKQY_400x400.jpg'
    const name = profile.displayName ? profile.displayName : 'John Smith'
    const description = ' Web & Mobile engineer, crypto investor, visionary leader'
    
    return (
      <View style={styles.headerContainer}>
        <View style={styles.userRow}>
          <Image
            style={styles.userImage}
            source={{ uri: image }}
          />
          <View style={styles.userNameRow}>
            <Text style={styles.userNameText}>{name}</Text>
          </View>
          <View style={styles.userBioRow}>
            <Text style={styles.userBioText}>
              {description}
            </Text>
          </View>
        </View>
        <View style={styles.socialRow}>
          <View>
            <Icon
              size={30}
              type="entypo"
              color="#3B5A98"
              name="facebook-with-circle"
              onPress={() => console.log('facebook')}
            />
          </View>
          <View style={styles.socialIcon}>
            <Icon
              size={30}
              type="entypo"
              color="#56ACEE"
              name="twitter-with-circle"
              onPress={() => console.log('twitter')}
            />
          </View>
          <View>
            <Icon
              size={30}
              type="entypo"
              color="#DD4C39"
              name="google--with-circle"
              onPress={() => console.log('google')}
            />
          </View>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderContactHeader()}
        <TabView
          navigationState={this.state}
          renderScene={SceneMap({
            first: this.firstRoute,
            second: this.secondRoute
          })}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: Dimensions.get('window').width }}
        />
      </View>
    )
  }
}
