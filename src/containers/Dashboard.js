import React, { Component } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { connect } from 'react-redux';

const styles = StyleSheet.create({
  base: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})

export default class Dashboard extends Component {
  static navigationOptions = { header: null }

  render() {
    // console.log('PROPS: ', this.props)
    return (
      <View style={styles.base}>
        <Text>{'HomeScreen(logged in)'}</Text>
        <Button
          onPress={() => this.props.navigation.navigate('SearchList')}
          title="Go to Search Results"
          color="#841584"
        />
      </View>
    );
  }
}