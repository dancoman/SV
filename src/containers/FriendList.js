import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
// import Profile from './Profile';
import DATA from '../api/Data';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  itemBlock: {
    flexDirection: 'row',
    paddingBottom: 10,
  },
  itemImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  itemMeta: {
    marginLeft: 10,
    justifyContent: 'center',
  },
  itemName: {
    fontSize: 20,
  },
  itemLastMessage: {
    fontSize: 14,
    color: "#111",
  },
  separator: {
    height: 0.5,
    width: "80%",
    alignSelf: 'center',
    backgroundColor: "#555"
  },
  header: {
    padding: 10,
  },
  headerText: {
    fontSize: 30,
    fontWeight: '900'
  }
});

export default class FriendList extends Component {
  // constructor(props) {
  //   super(props);

  static navigationOptions = {
    header: null,
  }

  state = {
    data: DATA,
    refreshing: false,
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 1000);
  }

  keyExtractor = (item, index) => {
    // console.log('TCL: keyExtractor -> index', index);
    return index.toString();
  }

  renderItem = (data) => {
    const { navigate } = this.props.navigation;
    // console.log('TCL: renderItem -> data', data);
    const { item, index } = data;
    return (
      <TouchableOpacity style={styles.itemBlock} onPress={() => navigate('FriendProfile', { item })}>
        <Image source={{ uri: item.picture }} style={styles.itemImage} />
        <View style={styles.itemMeta}>
          <Text style={styles.itemName}>{item.name}</Text>
          <Text style={styles.itemLastMessage}>{item.last_message}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderSeparator = () => <View style={styles.separator} />

  renderHeader = () => (
    <View style={styles.header}>
      <Text style={styles.headerText}>FriendList Example</Text>
    </View>
  )

  render() {
    return (
      <View style={styles.container}>
        {/* <FlatList
          keyExtractor={this.keyExtractor}
          data={this.state.data}
          renderItem={this.renderItem}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
        /> */}
      </View>
    );
  }
}
