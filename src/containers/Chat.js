// import React, { Component } from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// export default class Chat extends Component {
//   render() {
//     return (
//       <View style={styles.container} >
//         <Text>ChatScreen</Text>
//       </View>

//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center'
//   }
// })


import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Constants } from 'expo';
import { connect } from 'react-redux';
import { dispatch } from '../../App';

class Chat extends Component {
  render() {
    console.log('ChatProps', this.props)
    return (
      <View style={styles.container}>
        <Text style={styles.paragraph} onPress={() => dispatch.count.add()}>
          {this.props.count}
        </Text>
      </View>
    );
  }
}

export default connect(({ count }) => ({ count }))(Chat)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1'
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e'
  }
})
