import React from 'react'
import Dashboard from '../containers/Dashboard'
import Profile from '../containers/Profile'
import Recommendation from '../containers/Recommendation'
import FriendList from '../containers/FriendList'
import Chat from '../containers/Chat'
import SearchList from '../containers/SearchList'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import Icon from 'react-native-vector-icons/Ionicons'
import { createStackNavigator } from 'react-navigation';

const AppRouteConfigs = {
  Dashboard: {
    screen: createStackNavigator({
      Dashboard: {
        screen: Dashboard,
      },
      SearchList: {
        screen: SearchList,
      }
    }),
    navigationOptions: {
      tabBarLabel: 'Home',
      tabBarColor: '#FAFAFA',
      tabBarIcon: ({tintColor}) => (
        <Icon name="ios-globe" color={tintColor} size={24} />
      )
    }
  },
  FriendList: {
    screen: createStackNavigator({
      FriendList: {
        screen: FriendList,
      },
      FriendProfile: {
        screen: Profile,
      },
    }),
    navigationOptions: {
      tabBarLabel: 'Friends',
      tabBarColor: '#FAFAFA',
      tabBarIcon: ({tintColor}) => (
        <Icon name="ios-contacts" color={tintColor} size={24} />
      )
    }
  },
  Recommedation: {
    screen: Recommendation,
    navigationOptions: {
      tabBarLabel: 'Recommend',
      tabBarColor: '#FAFAFA',
      tabBarIcon: ({tintColor}) => (
        <Icon name="ios-add-circle" color={tintColor} size={24} />
      )
    }
  },
  Chat: {
    screen: Chat,
    navigationOptions: {
      tabBarLabel: 'Chat',
      tabBarColor: '#FAFAFA',
      tabBarIcon: ({tintColor}) => (
        <Icon name="ios-chatbubbles" color={tintColor} size={24} />
      )
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      tabBarLabel: 'Profile',
      tabBarColor: '#FAFAFA',
      tabBarIcon: ({tintColor}) => (
        <Icon name="ios-person" color={tintColor} size={24} />
      )
    }
  }
};

const LoggedInNavigator = createMaterialBottomTabNavigator(
  AppRouteConfigs,
  {
    initialRouteName: 'Dashboard',
    activeTintColor: 'black',
    shifting: true,
    inactiveTintColor: 'blue'
  }
);

export default LoggedInNavigator
