import { createStackNavigator } from 'react-navigation'

import Login from '../containers/Login'

const LoggedOutNavigator = createStackNavigator({
  Login: {
    screen: Login
  }
});

export default LoggedOutNavigator
