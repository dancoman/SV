import { dispatch } from '../../App' // 0.1.6

export const count = {
  state: 0,
  reducers: {
    add: (state) => state + 1
  },
  // effects: {
  //   addAsync: async () => {
  //     dispatch.count.add();
  //   }
  // },
}