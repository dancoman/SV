import React, { Component } from 'react'
import { View, ActivityIndicator, StyleSheet, AsyncStorage } from 'react-native'
import { getRootNavigator } from './loginNavigator'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { connect } from 'react-redux';


@firebaseConnect()
@connect((state) => ({ profile: state.firebase.profile }))

export default class Root extends Component {

  // state = {
  //   loading: true,
  // };

  // componentDidMount() {
  //   if () {
  //     this.setState({ loggedIn: true, loading: false })
  //   }
  // }
 
  AS = async () => {
    console.log('insideAsync')
    try {
      const keys = await AsyncStorage.getAllKeys()
      console.log('TCL: Root -> GT -> keys', keys);
      // const values = await AsyncStorage.multiGet(keys)
      // console.log('TCL: Root -> GT -> values', values);
    } catch (error) {
      console.log('Error: ', error);
    }
  }

  render() {
    console.log('profile', this.props.profile)
    // this.AS()
    if (!this.props.profile.isLoaded) {
      return (
        <View style={styles.base}>
          <ActivityIndicator size='small' />
        </View>
      )
    }
    
    const RootNavigator = getRootNavigator(this.props.profile.isLoaded)
    return <RootNavigator />
  }
}

const styles = StyleSheet.create({
  base: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
