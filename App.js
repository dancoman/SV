import React, { Component } from 'react'
import { AsyncStorage } from 'react-native'
import { Provider } from 'react-redux'
import { init } from '@rematch/core'
import createRematchPersist, { getPersistor } from '@rematch/persist'
import reduxReset from 'redux-reset'
import { createStore, compose } from 'redux'
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase'
import { reduxFirestore, firestoreReducer } from 'redux-firestore'
import createLoadingPlugin from '@rematch/loading';
import { PersistGate } from 'redux-persist/lib/integration/react'
import * as models from './src/rematch/models'
import firebase from './src/config/firebase'
import Root from './src/Root'


const loadingPlugin = createLoadingPlugin();

const persistPlugin = createRematchPersist({
  storage: AsyncStorage,
  whitelist: ['count']
})

const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase,
    {
      userProfile: 'users', // where profiles are stored in database
      // presence: 'presence', // where list of online users is stored in database - must use .login
      useFirestoreForProfile: true,
      enableLogging: false,
      enableRedirectHandling: false
      // attachAuthIsReady: true
    }),
  reduxReset(),
  reduxFirestore(firebase)
)(createStore)


export const store = init({
  models,
  plugins: [persistPlugin, loadingPlugin],
  redux: {
    createStore: createStoreWithFirebase,
    reducers: {
      firebase: firebaseReducer,
      firestore: firestoreReducer
    }
  }
});


export const { dispatch } = store
// console.log('TCL: store', store.getState());
export const persistor = getPersistor()
// console.log('TCL: persistor', persistor);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Root />
        </PersistGate>
      </Provider>
    );
  }
}
